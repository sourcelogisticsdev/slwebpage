
/********************** Backgrounds **********************/

		
	html { 
		background: #f8f8f8 url("http://sourcelogistics.com/wp-content/uploads/2013/06/layout.jpg") center top no-repeat;
	}
	
		body { 
		background-position: center top;
		background-repeat: repeat;
		background-image: url('http://sourcelogistics.com/wp-content/themes/bretheon/images/overlays/2.png');
	}
			
	#Wrapper {
		background-color: #fff;
	}
	

/********************** Fonts **********************/

 	body, button, input[type="submit"], input[type="reset"], input[type="button"],
	input[type="text"], input[type="password"], input[type="email"], textarea, select {
		font-family: Titillium Web, Arial, Tahoma, sans-serif;
		font-weight: 300;
	}
	
	#menu > ul > li > a, a.button, input[type="submit"], input[type="reset"], input[type="button"], .ui-tabs .ui-tabs-nav li a, .post .desc .meta {
		font-family: Patua One, Arial, Tahoma, sans-serif;
		font-weight: 300;
	}
	
	h1 {
		font-family: Patua One, Arial, Tahoma, sans-serif;
		font-weight: 100;
	}
	
	h2 {
		font-family: Patua One, Arial, Tahoma, sans-serif;
		font-weight: 100;
	}
	
	h3 {
		font-family: Patua One, Arial, Tahoma, sans-serif;
		font-weight: 100;
	}
	
	h4 {
		font-family: Patua One, Arial, Tahoma, sans-serif;
		font-weight: 300;
	}
	
	h5 {
		font-family: Patua One, Arial, Tahoma, sans-serif;
		font-weight: 300;
	}
	
	h6 {
		font-family: Patua One, Arial, Tahoma, sans-serif;
		font-weight: 300;
	}


/********************** Font sizes **********************/

/* Body */

	body {
		font-size: 14px;
				line-height: 21px;		
	}
	
/* Headings */
	
	h1 { 
		font-size: 50px;
				line-height: 50px;
	}
	
	h2 { 
		font-size: 42px;
				line-height: 42px;
	}
	
	h3 {
		font-size: 28px;
				line-height: 30px;
	}
	
	h4 {
		font-size: 24px;
				line-height: 27px;
	}
	
	h5 {
		font-size: 20px;
				line-height: 22px;
	}
	
	h6 {
		font-size: 14px;
				line-height: 18px;
	}
	
/* Footer */
	#Footer {
	    font-size: 90%;
	    line-height: 122%;
	}
	
/* Grey notes */

	.Recent_comments li span.date, .Latest_posts span.date {
		font-size: 92%;
	    line-height: 130%;
	}